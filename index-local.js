const { Server } = require("socket.io");
const express = require("express");
const codegenerator = require("./external_modules/codegenerator.ag.js");
const socket_multiplayer = require("./external_modules/socket.ag.js");
const app = express();
const dotenv = require("dotenv");
const cors = require("cors");
const PanjatPinang = require("./external_modules/panjatpinang-server.ag");

dotenv.config();

var server = app.listen(3000, () => {
    console.log("Listening port 3000");
});

var option = {
    cors: {
        origin: '*',
        credential: true
    }
}

const io = new Server(server, option);

io.on("connection", connection);

const ag_codegenerator = codegenerator.Generator();

const ag_socket = socket_multiplayer.Init(io, {
    maxPlayerEachRoom: 4,
    plugins: {
        code_generator: ag_codegenerator
    },
    gameServer: new PanjatPinang(io)
});

function connection(socket) {
    socket.on("connectPlayer", (data) => {
        ag_socket.connectPlayer(socket, data);
    });

    socket.on("createRoom", (data) => {
        ag_socket.createRoom(socket, data);
    });

    socket.on("joinRoom", (data) => {
        ag_socket.joinRoom(socket, data);
    });

    socket.on("joinRoomBOT", (data) => {
        ag_socket.joinRoomBOT(socket, data);
    });

    socket.on("startRoom", (data) => {
        ag_socket.startRoom(socket, data);
    });

    socket.on("disconnect", () => {
        ag_socket.playerDisconnect(socket);
    });

    socket.on("player_update", (data) => {
        ag_socket.gameUpdate(data);
    });

    socket.on("checkPodiumWinners", (data) => {
        ag_socket.checkPodiumWinners(data);
    });

    socket.on("setWinner", (data) => {
        ag_socket.setWinner(data);
    });
}