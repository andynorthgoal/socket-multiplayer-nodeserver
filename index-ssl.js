var fs = require('fs');
var http = require('http');
var https = require('https');


const { Server } = require("socket.io");
const express = require("express");
const codegenerator = require("./external_modules/codegenerator.ag.js");
const socket_multiplayer = require("./external_modules/socket.ag.js");
const app = express();
const dotenv = require("dotenv");
const cors = require("cors");
const PanjatPinang = require("./external_modules/panjatpinang-server.ag");

dotenv.config();

app.use(cors());

var privateKey = fs.readFileSync('/etc/letsencrypt/live/multiplayer-instance.andiwaluyo.com/privkey.pem', 'utf8');
var certificate = fs.readFileSync('/etc/letsencrypt/live/multiplayer-instance.andiwaluyo.com/cert.pem', 'utf8');

var credentials = { key: privateKey, cert: certificate };

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(3000);

var server = httpsServer.listen(3001);

app.get('/', (req, res) => {
    res.send("<h4>Multiplayer Game Server</h4>");
});

var option = {
    cors: {
        origin: '*',
        credential: true
    }
}

const io = new Server(server, option);

io.on("connection", connection);

const ag_codegenerator = codegenerator.Generator();

const ag_socket = socket_multiplayer.Init(io, {
    maxPlayerEachRoom: 4,
    plugins: {
        code_generator: ag_codegenerator
    },
    gameServer: new PanjatPinang(io)
});

function connection(socket) {
    socket.on("connectPlayer", (data) => {
        ag_socket.connectPlayer(socket, data);
    });

    socket.on("createRoom", (data) => {
        ag_socket.createRoom(socket, data);
    });

    socket.on("joinRoom", (data) => {
        ag_socket.joinRoom(socket, data);
    });

    socket.on("joinRoomBOT", (data) => {
        ag_socket.joinRoomBOT(socket, data);
    });

    socket.on("startRoom", (data) => {
        ag_socket.startRoom(socket, data);
    });

    socket.on("disconnect", () => {
        ag_socket.playerDisconnect(socket);
    });

    socket.on("player_update", (data) => {
        ag_socket.gameUpdate(data);
    });

    socket.on("checkPodiumWinners", (data) => {
        ag_socket.checkPodiumWinners(data);
    });

    socket.on("setWinner", (data) => {
        ag_socket.setWinner(data);
    });
}