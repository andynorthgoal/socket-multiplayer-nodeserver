class PanjatPinang {
    constructor(socketIO) {
        this.socketIO = socketIO;
    }

    countdownw() {
        return {
            start: (roomId) => {
                var countdownTimer = 5;
                var countDown = setInterval(() => {
                    this.socketIO.to(roomId).emit("countdownGameStart", {
                        action: "countdown",
                        data: countdownTimer
                    });
                    countdownTimer--;
                    if (countdownTimer < 0) {
                        clearInterval(countDown);
                    }
                }, 1000);
            }
        }
    }
}

module.exports = PanjatPinang;