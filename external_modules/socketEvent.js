module.exports = {
    init: function(io, game, humanName, client, promisify) {
        const redisRoomList = promisify(client.keys).bind(client);
        const redisCheckRoom = promisify(client.exists).bind(client);
        const redisGetData = promisify(client.get).bind(client);
        const redisSetData = (room, data, callback) => {
            client.setex(room, 1800, JSON.stringify(data), (err, res) => {
                callback(err, res);
            });
        }
        const generateCode = (length) => {
            var result = "";
            var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        let conneted_players = {};
        let list_gameplay = {};
        let tanding_acak_queue = {}

        return {
            //CREATE ROOM HANDLER
            create_room: async function(socket, data) {

                var roomType = data.roomType;
                var roomId = generateCode(8);

                while (true) {
                    if (await redisCheckRoom(roomId) == 1) {
                        roomId = generateCode(8);
                    } else {
                        break;
                    }
                }

                var roomCheck = await redisCheckRoom(roomId);
                if (roomCheck == 1) {
                    io.to(socket.id).emit('create_room', {
                        action: false,
                        message: "Maaf room sdah tersedia!",
                    });
                    return;
                }

                if (roomType == 'acak') {
                    roomId = `acak-${roomId}`
                }

                var players = {}
                players[socket.id] = {
                    id: socket.id,
                    playerType: "p1",
                    playerName: data.playerName,
                    roomId: roomId,
                    roomType: roomType
                }

                conneted_players[socket.id] = {
                    id: socket.id,
                    playerType: "p1",
                    playerName: data.playerName,
                    roomId: roomId,
                    roomType: roomType
                };

                var roomStatus = {
                    roomId: roomId,
                    game_started: false,
                    room_master: socket.id,
                    roomType: roomType,
                    players: players,
                }

                redisSetData(roomId, roomStatus, (err, res) => {
                    if (!err) {
                        if (res == `OK`) {
                            var joinCode = `${roomType}-${roomId}`;
                            if (roomType == 'acak') {
                                joinCode = roomId;
                            }
                            socket.join(joinCode);
                            io.to(socket.id).emit('create_room', {
                                action: true,
                                message: "Create room Success!",
                                player: players[socket.id],
                                room: roomStatus
                            });
                        }
                    }
                });
            },

            //CREATE ROOM HANDLER
            close_room: function(socket, data) {
                let playerData = conneted_players[socket.id];
                if (!playerData) {
                    return;
                }

                client.del(playerData.roomId, async function(err, response) {
                    if (response == 1) {
                        //console.log("Deleted Successfully!");
                        delete conneted_players[socket.id];
                    } else {
                        //console.log("Cannot delete")
                    }
                });
            },

            //JOIN ROOM HANDLER
            join_room: async function(socket, data) {
                var roomType = data.roomType;
                var roomId = data.roomId;

                var roomCheck = await redisCheckRoom(roomId);
                if (roomCheck == 0) {
                    if (roomType != "acak") {
                        io.to(socket.id).emit('join_room', {
                            action: false,
                            message: "Room not found!",
                        });
                    }
                    return;
                }

                var getRoom = await redisGetData(roomId);
                if (!getRoom) {
                    if (roomType != "acak") {
                        io.to(socket.id).emit('join_room', {
                            action: false,
                            message: "Can't get data!",
                        });
                    }
                    return;
                }

                var room = JSON.parse(getRoom);

                if (room.game_started) {
                    if (roomType != "acak") {
                        io.to(socket.id).emit('join_room', {
                            action: false,
                            message: "Room already started!",
                        });
                    }
                    return;
                }

                room[`game_started`] = true;
                room[`players`][socket.id] = {
                    id: socket.id,
                    playerType: "p2",
                    playerName: data.playerName,
                    roomId: roomId,
                    roomType: roomType
                }

                conneted_players[socket.id] = room[`players`][socket.id];

                redisSetData(roomId, room, (err, res) => {
                    if (!err) {
                        if (res == `OK`) {
                            var joinCode = `${roomType}-${roomId}`;

                            if (roomType == "acak") {
                                joinCode = roomId;
                            }

                            socket.join(joinCode);
                            io.to(socket.id).emit('join_room', {
                                action: true,
                                message: "Join room Success!",
                                player: room[`players`][socket.id],
                                room: room
                            });

                            if (roomType == "acak") {
                                data.matchMaker.stopMatchMaking();
                            }
                        }
                    }
                });
            },

            //TANDING ACAK HANDLER
            tanding_acak: async function(socket, data) {
                switch (data.request) {
                    case 'cancel':
                        if (socket.id in tanding_acak_queue) {
                            tanding_acak_queue[socket.id].stopMatchMaking();
                            delete tanding_acak_queue[socket.id];
                        } else {
                            //handle not in queue
                            return;
                        }
                        break;
                    case 'match_found':
                        tanding_acak_queue[socket.id].stopMatchMaking();
                        break;
                    case 'init_bot':
                        var getRoom = await redisGetData(data.roomId);

                        var room = JSON.parse(getRoom);

                        var BOT_NAME = humanName.allRandom();

                        room[`game_started`] = true;
                        room[`players`][`BOT.${BOT_NAME}`] = {
                            id: `BOT.${BOT_NAME}`,
                            playerType: "p2",
                            playerName: BOT_NAME,
                            roomId: data.roomId,
                            roomType: 'acak'
                        }

                        redisSetData(data.roomId, room, (err, res) => {
                            if (!err) {
                                if (res == `OK`) {
                                    io.to(socket.id).emit("match_making", {
                                        action: `bot_ready`,
                                        room: room
                                    });

                                    this.init_gameplay(socket, {
                                        action: "start_game",
                                        roomType: room.roomType,
                                        roomId: room.roomId,
                                        roomPlayers: room.players,
                                    });
                                }
                            }
                        });
                        break;
                    default:
                        if (socket.id in tanding_acak_queue) {
                            //handle already queued
                            return;
                        }

                        var matchMaker = game.matchMaking({
                            acakTimer: 15,
                            limitCreateRoom: 10,
                        });

                        tanding_acak_queue[socket.id] = matchMaker;

                        var findRoom = async() => {
                            var roomKeys = await redisRoomList("*");
                            var newArray = [];
                            roomKeys.forEach(element => {
                                if (element.includes('acak')) {
                                    newArray.push(element);
                                }
                            });

                            if (newArray.length > 0) {
                                var roomId = newArray[0];
                                this.join_room(socket, {
                                    roomId: roomId,
                                    roomType: "acak",
                                    playerName: data.playerName,
                                    matchMaker: matchMaker
                                });
                            }
                        }
                        matchMaker.startMatchMaking(async(a) => {
                            var tick = a.tick;
                            io.to(socket.id).emit("match_making", {
                                tick: tick
                            });

                            switch (a.state) {
                                case "finding_other_players":
                                    if (tick % 2 == 0) {
                                        findRoom();
                                    }
                                    break;
                                case "create_room":
                                    this.create_room(socket, {
                                        roomType: "acak",
                                        playerName: data.playerName
                                    });
                                    io.to(socket.id).emit("match_making", {
                                        action: "create_room",
                                        tick: tick
                                    });
                                    break;
                                case "play_with_bot":
                                    io.to(socket.id).emit("match_making", {
                                        action: "play_with_bot",
                                        tick: 0
                                    });
                                    break;
                            }
                        });
                        break;
                }
            },

            //INIT GAMEPLAY HANDLER
            init_gameplay: function(socket, data) {
                var joinCode = `${data.roomType}-${data.roomId}`;
                if (data.roomType == "acak") {
                    joinCode = data.roomId;
                }
                switch (data.action) {
                    case "start_game":
                        var gameplay = game.gamePlay(io, joinCode, data.roomPlayers, {
                            prepCountdown: 5, //seconds
                            gameplayTimer: 30, //seconds
                            spawnItemTimer: 5,
                        });

                        io.to(joinCode).emit("gameplay", {
                            action: `game_ready`
                        });

                        list_gameplay[joinCode] = gameplay;

                        setTimeout(() => {
                            gameplay.initPlayers();
                            gameplay.startGame();
                        }, 3000);

                        break;
                    case "retry_game":
                        list_gameplay[joinCode].restartGame();
                        break;
                    case "end_game":
                        list_gameplay[joinCode].stopGame(false);
                        break;
                    case "force_end_game":
                        list_gameplay[joinCode].stopGame();
                        delete list_gameplay[joinCode];
                        break;
                }
            },

            //REMATCH REQUEST HANDLER
            rematch: function(io, socket, data) {
                var joinCode = `${data.roomType}-${data.roomId}`;
                if (data.roomType == 'acak') {
                    joinCode = data.roomId;
                }
                io.to(joinCode).emit("rematch", {
                    status: true,
                    data: {
                        rematchReady: data.rematchReady
                    }
                });
            },

            //EMOTICON HANDLER
            emoticon: function(socket, data) {
                var joinCode = `${data.roomType}-${data.roomId}`;
                if (data.roomType == "acak") {
                    joinCode = data.roomId;
                }
                io.to(joinCode).emit("emoticon", {
                    action: true,
                    emoticonIndex: data.index,
                    memberId: data.memberId,
                });
            },

            //GRAB ITEMs HANDLER
            grab_item: function(socket, data) {
                var gameAction = game.gameAction(io);
                gameAction.onGrab(data);
            },

            //PLAYER DISCONNECTED HANDLER
            disconnect: function(io, socket) {
                let playerData = conneted_players[socket.id];

                if (!playerData) {
                    return;
                }

                var joinCode = `${playerData.roomType}-${playerData.roomId}`;

                if (playerData.roomType == `acak`) {
                    joinCode = playerData.roomId;
                }

                io.to(joinCode).emit("gameplay", {
                    action: `matchovercallback`,
                    status: `end_game_force`
                });

                client.del(playerData.roomId, async function(err, response) {
                    if (response == 1) {
                        //console.log("Deleted Successfully!");
                    } else {
                        //console.log("Cannot delete")
                    }
                });

                if (playerData.playerType == "p2") {
                    if (joinCode in list_gameplay) {
                        list_gameplay[joinCode].stopGame(true);
                        delete list_gameplay[joinCode];
                    }
                }

                socket.leave(joinCode);
                delete conneted_players[socket.id];
            },
        }
    }
}