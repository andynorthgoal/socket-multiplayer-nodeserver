module.exports = {
    allRandom: function() {
        const listName = [
            "Yudha",
            "Angga",
            "Budi",
            "Bimo",
            "Tomi",
            "Hendi",
            "Fikri",
            "Rian",
            "Andi",
            "Iwan",
            "Vian",
            "Edwin",
            "Rahma",
            "Dani",
            "Hendra",
            "Lia",
            "Febi",
            "Egi",
            "Anggi",
            "Indri",
            "Yeni",
            "Ana",
            "Nina",
            "Novi",
            "Diana",
            "Riska",
            "Mega",
            "Dina",
            "Eca",
            "Deby"
        ]

        var rIndex = Math.floor(Math.random() * listName.length);

        return listName[rIndex];
    }
}