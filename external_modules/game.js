module.exports = {
    gamePlay: function(io, roomId, players, config) {
        var preparationCoroutine = null;
        var gameplayCoroutine = null;
        var isGameStarted = false;
        var isGameOver = false;

        var itemSpawner = (despawnCallback, spawnCallback) => {
            let tempSpawner = [];
            let tempItems = [];

            var oldSpawners = [];
            var oldItems = [];

            despawnCallback(oldSpawners, oldItems);
            this.despawnCb = despawnCallback;

            setTimeout(() => {
                var maxitem = Math.floor(
                    Math.random() *
                    (6 - 4 + 1) + 4
                );
                var totalObstaclesItem = 2;
                var totalNormalItem = maxitem - totalObstaclesItem;

                while (tempSpawner.length < maxitem) {
                    var r = Math.floor(Math.random() * 9);
                    if (tempSpawner.indexOf(r) === -1) tempSpawner.push(r);
                }

                var itemNormal = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
                var itemObstacle = [10, 11, 12];

                var shuffle = function(o) {
                    //v1.0
                    for (
                        var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x
                    );
                    return o;
                };

                shuffle(itemNormal);
                shuffle(itemObstacle);

                for (let index = 0; index < totalNormalItem; index++) {
                    tempItems.push(itemNormal[index]);
                }

                for (let index = 0; index < totalObstaclesItem; index++) {
                    tempItems.push(itemObstacle[index]);
                }

                oldSpawners = tempSpawner;
                oldItems = tempItems;
                var spawners = tempSpawner;
                var items = tempItems;

                spawnCallback(spawners, items);
            }, 500);
        }

        var gameFlow = () => {
            //START COUNT DOWN BEFORE MATCH STARTED
            var prepCountdown = config.prepCountdown;
            preparationCoroutine = setInterval(() => {
                if (isGameOver) {
                    clearInterval(preparationCoroutine);
                    preparationCoroutine = null;
                    return;
                }

                prepCountdown--;
                if (prepCountdown >= 1) {
                    io.to(roomId).emit("gameplay", {
                        action: `prepcallback`,
                        state: `running`,
                        gameStarted: isGameStarted,
                        data: {
                            tick: prepCountdown
                        }
                    });
                    return;
                }

                if (prepCountdown <= 0) {
                    io.to(roomId).emit("gameplay", {
                        action: `prepcallback`,
                        state: `stopped`,
                        gameStarted: isGameStarted,
                        data: {
                            players: players
                        }
                    });

                    clearInterval(preparationCoroutine);
                    preparationCoroutine = null;

                    //GAME STARTED
                    isGameStarted = true;
                    itemSpawner(
                        (spawners, items) => { //Despawn Items
                            io.to(roomId).emit("gameplay", {
                                action: "despawnitemcallback",
                                data: { spawners: spawners, items: items },
                            });
                        },
                        (spawners, items) => { //Spawn Items
                            io.to(roomId).emit("gameplay", {
                                action: "spawnitemcallback",
                                data: { spawners: spawners, items: items },
                            });
                        });
                    var gameplayTimer = config.gameplayTimer;
                    var spawnItemTimer = config.spawnItemTimer;
                    gameplayCoroutine = setInterval(() => {
                        if (isGameOver) {
                            clearInterval(gameplayCoroutine);
                            gameplayCoroutine = null;
                            return;
                        }
                        //SPAWN ITEMS COROUTINE
                        if (gameplayTimer % spawnItemTimer == 0) {
                            itemSpawner(
                                (spawners, items) => { //Despawn Items
                                    io.to(roomId).emit("gameplay", {
                                        action: "despawnitemcallback",
                                        data: { spawners: spawners, items: items },
                                    });
                                },
                                (spawners, items) => { //Spawn Items
                                    io.to(roomId).emit("gameplay", {
                                        action: "spawnitemcallback",
                                        data: { spawners: spawners, items: items },
                                    });
                                });
                        }
                        gameplayTimer--;
                        if (gameplayTimer >= 1) {
                            io.to(roomId).emit("gameplay", {
                                action: `roundcallback`,
                                state: `running`,
                                data: {
                                    tick: gameplayTimer
                                }
                            });
                            return;
                        }

                        if (gameplayTimer <= 0) {
                            io.to(roomId).emit("gameplay", {
                                action: `roundcallback`,
                                state: `stopped`,
                                data: {
                                    players: players
                                }
                            });
                            clearInterval(gameplayCoroutine);
                            gameplayCoroutine = null;
                            if (!isGameOver) {
                                gameFlow();
                            }
                        }
                    }, 1000);
                }
            }, 1000);
        }

        var stopMachine = () => {
            isGameOver = true;

            clearInterval(preparationCoroutine);
            clearInterval(gameplayCoroutine);

            preparationCoroutine = null;
            gameplayCoroutine = null;
        }

        return {
            initPlayers: function() {
                io.to(roomId).emit("gameplay", {
                    action: `initplayerscallback`,
                    data: {
                        players: players
                    }
                });
            },

            startGame: function() {
                gameFlow();
            },

            restartGame: function() {
                preparationCoroutine = null;
                gameplayCoroutine = null;
                isGameStarted = false;
                isGameOver = false;

                gameFlow();

                io.to(roomId).emit("gameplay", {
                    action: `matchovercallback`,
                    status: `restart`
                });
            },

            stopGame: function() {
                stopMachine();
            },

            stopGame: function(oneOfPlayerExit) {
                stopMachine();

                if (oneOfPlayerExit) {
                    io.to(roomId).emit("gameplay", {
                        action: `matchovercallback`,
                        status: `end_game_force`
                    });
                } else {
                    io.to(roomId).emit("gameplay", {
                        action: `matchovercallback`,
                        status: `end_game`
                    });
                }
            }
        }
    },

    gameAction: function(io) {
        return {
            onGrab: function(data) {

                var p1_score = data.match_points.p1.score;
                var p2_score = data.match_points.p2.score;
                var p1_totalScore = data.match_points.p1.totalScore;
                var p2_totalScore = data.match_points.p2.totalScore;

                console.log(`Game Server Incoming Point Before use handle: ${data.point}`);
                var mvl = (data.point) > 0 ? 1 : -1;
                console.log(`Game Server Incoming Point After use handler: ${data.point}`);

                if (data.playerType == "p1") {
                    p1_score = p1_score + mvl;
                    if (p1_score < 0) p1_score = 0;
                    p1_totalScore = p1_totalScore + p1_score;
                } else if (data.playerType == "p2") {
                    p2_score = p2_score + mvl;
                    if (p2_score < 0) p2_score = 0;
                    p2_totalScore = p2_totalScore + p2_score;
                }

                var joinCode = `${data.roomType}-${data.roomId}`;
                if (data.roomType == 'acak') {
                    joinCode = data.roomId;
                }

                io.to(joinCode).emit("grab_item", {
                    action: true,
                    data: {
                        playerType: data.playerType,
                        incomingPoint: mvl,
                        match_points: {
                            p1: {
                                score: p1_score,
                                totalScore: p1_totalScore,
                            },
                            p2: {
                                score: p2_score,
                                totalScore: p2_totalScore,
                            }
                        },
                        item: data.item
                    },
                });
            }
        }
    },

    matchMaking: function(config) {
        var pertandinganAcakCoroutine = null;
        var isMatchFound = false;
        return {
            startMatchMaking: function(callback) {
                var countdown = config.acakTimer;
                pertandinganAcakCoroutine = setInterval(() => {
                    if (isMatchFound) {
                        clearInterval(pertandinganAcakCoroutine);
                        pertandinganAcakCoroutine = null;
                        callback({
                            state: "play_with_player",
                            tick: countdown
                        });
                        return;
                    }

                    countdown--;

                    if (countdown > config.limitCreateRoom) {
                        callback({
                            state: "finding_other_players",
                            tick: countdown
                        });
                        return;
                    }

                    if (countdown >= 1 && countdown <= config.limitCreateRoom) {
                        if (countdown == config.limitCreateRoom) {
                            callback({
                                state: "create_room",
                                tick: countdown
                            });
                        }
                        callback({
                            state: "standby",
                            tick: countdown
                        });
                        return;
                    }

                    if (countdown <= 0) {
                        callback({
                            state: "play_with_bot",
                            tick: 0
                        });

                        clearInterval(pertandinganAcakCoroutine);
                        pertandinganAcakCoroutine = null;
                    }
                }, 1000);
            },

            stopMatchMaking: function() {
                clearInterval(pertandinganAcakCoroutine);
                pertandinganAcakCoroutine = null;
            }
        }
    }
}