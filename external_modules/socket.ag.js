module.exports = {
    Init: function(socketIO, config) {

        var players = {};
        var players_bot = {};
        var matches = {};

        var maxPlayer = 2;
        var maxPlayerAcakRoom = 1;

        var gameServer = config.gameServer;
        var codeGenerator = config.plugins["code_generator"];

        return {
            connectPlayer: function(socket, data) {
                var playerId = socket.id;
                if (playerId in players) {
                    socketIO.to(playerId).emit("connectPlayer", {
                        action: "failed",
                        message: "Browser or Device already connected!"
                    });
                    return;
                }

                players[playerId] = {
                    id: playerId,
                    playing: 0,
                    room_owner: 0,
                    room_id: "",
                }

                socketIO.to(playerId).emit("connectPlayer", {
                    action: "success",
                    message: "Connection success!",
                    data: {
                        playerId: playerId
                    }
                });
            },

            createRoom: function(socket, data) {

                var playerId = data.playerId;

                if (!(playerId in players)) {
                    socketIO.to(playerId).emit("createRoom", {
                        action: "failed",
                        message: "You forcing me, how dare you! We are not connected yet!",
                        suggest: "Connect using connectPlayer() instead of direct connect!"
                    });
                    return;
                }

                var generatedCode = codeGenerator.start(4);
                while (generatedCode in matches) {
                    generatedCode = codeGenerator.start(4);
                }

                var roomId = generatedCode;

                players[playerId] = {
                    id: playerId,
                    playing: 1,
                    room_owner: 1,
                    room_id: roomId,
                    player_name: data.playerName,
                    character: data.character
                }

                matches[roomId] = {
                    id: roomId,
                    players: [
                        players[playerId]
                    ],
                    is_playing: false,
                }

                socket.join(roomId);

                socketIO.to(roomId).emit("createRoom", {
                    action: "success",
                    message: "You has been create new match room!",
                    data: {
                        room: matches[roomId]
                    }
                });

            },

            joinRoom: function(socket, data) {
                var playerId = data.playerId;
                var roomId = data.roomId;

                if (!players.hasOwnProperty(playerId)) {
                    socketIO.to(playerId).emit("joinRoom", {
                        action: "failed",
                        message: "You forcing me, how dare you! We are not connected yet!",
                        suggest: "Connect using connectPlayer() instead of direct connect!"
                    });
                    return;
                }

                if (!matches.hasOwnProperty(roomId)) {
                    socketIO.to(playerId).emit("joinRoom", {
                        action: "failed",
                        message: "Room not found!",
                        suggest: "Change your match code!"
                    });
                    return;
                }

                var room = matches[roomId];

                if (room.is_playing) {
                    socketIO.to(playerId).emit("joinRoom", {
                        action: "failed",
                        message: "Room started!",
                        suggest: "Find another rooms!"
                    });
                    return;
                }

                if (room.players.length >= config.maxPlayerEachRoom) {
                    socketIO.to(playerId).emit("joinRoom", {
                        action: "failed",
                        message: "Room full!",
                        suggest: "Find another rooms!"
                    });
                    return;
                }

                players[playerId] = {
                    id: playerId,
                    playing: 1,
                    room_owner: 0,
                    room_id: roomId,
                    player_name: data.playerName,
                    character: data.character
                }

                var curPlayers = matches[roomId].players;
                curPlayers.push(players[playerId]);

                matches[roomId] = {
                    id: roomId,
                    players: curPlayers,
                    is_playing: false,
                }

                socket.join(roomId);

                socketIO.to(roomId).emit("joinRoom", {
                    action: "success",
                    message: "You has been join a match room!",
                    data: {
                        room: matches[roomId]
                    }
                });

            },

            joinRoomBOT: function(socket, data) {
                var playerId = data.playerId;
                var roomId = data.roomId;

                if (!matches.hasOwnProperty(roomId)) {
                    socketIO.to(playerId).emit("joinRoom", {
                        action: "failed",
                        message: "Room not found!",
                        suggest: "Change your match code!"
                    });
                    return;
                }

                var room = matches[roomId];

                if (room.is_playing) {
                    socketIO.to(playerId).emit("joinRoom", {
                        action: "failed",
                        message: "Room started!",
                        suggest: "Find another rooms!"
                    });
                    return;
                }

                if (room.players.length >= config.maxPlayerEachRoom) {
                    socketIO.to(playerId).emit("joinRoom", {
                        action: "failed",
                        message: "Room full!",
                        suggest: "Find another rooms!"
                    });
                    return;
                }

                players[playerId] = {
                    id: playerId,
                    playing: 1,
                    room_owner: 0,
                    room_id: roomId,
                    player_name: data.playerName,
                    character: data.character
                }

                var curPlayers = matches[roomId].players;
                curPlayers.push(players[playerId]);

                matches[roomId] = {
                    id: roomId,
                    players: curPlayers,
                    is_playing: false,
                }

                socket.join(roomId);

                socketIO.to(roomId).emit("joinRoom", {
                    action: "success",
                    message: "You has been join a match room!",
                    data: {
                        room: matches[roomId]
                    }
                });

            },

            startRoom: function(socket, data) {
                var playerId = data.playerId;
                var roomId = data.roomId;

                if (!playerId in players) {
                    socketIO.to(playerId).emit("startRoom", {
                        action: "failed",
                        message: "You forcing me, how dare you! We are not connected yet!",
                        suggest: "Connect using connectPlayer() instead of direct connect!"
                    });
                    return;
                }

                if (!roomId in matches) {
                    socketIO.to(playerId).emit("startRoom", {
                        action: "failed",
                        message: "Room not found!",
                        suggest: "Change your match code!"
                    });
                    return;
                }

                var room = matches[roomId];

                if (room.is_playing) {
                    socketIO.to(playerId).emit("startRoom", {
                        action: "failed",
                        message: "Room started!",
                        suggest: "Find another rooms!"
                    });
                    return;
                }

                if (room.players.length < maxPlayer) {
                    socketIO.to(playerId).emit("startRoom", {
                        action: "failed",
                        message: "Minimal 2 Player!",
                        suggest: "Find another players!"
                    });
                    return;
                }

                matches[roomId]["is_playing"] = true;

                socketIO.to(roomId).emit("startRoom", {
                    action: "success",
                    message: "GameStarted!",
                    data: {
                        room: room
                    }
                });

                gameServer.countdownw().start(roomId);
            },

            gameUpdate: function(data) {
                var roomId = data.roomId;
                var playerId = data.playerId;
                var heightPosition = data.heightPosition;
                socketIO.to(roomId).emit("player_update", {
                    action: "on_move",
                    message: "Players has move!",
                    data: {
                        playerId: playerId,
                        heightPosition: heightPosition
                    }
                });
            },

            setWinner: function(data) {
                var roomId = data.roomId;
                var playerId = data.playerId;
                if (typeof matches[roomId]["winners"] == "undefined") {
                    var newArr = [];
                    newArr.push(players[playerId]);
                    matches[roomId]["winners"] = newArr;
                    socketIO.to(roomId).emit("setWinner", {
                        action: "success",
                        initiator: playerId,
                        data: {
                            winners: matches[roomId]["winners"]
                        }
                    });
                } else {
                    matches[roomId]["winners"].push(players[playerId]);
                    socketIO.to(roomId).emit("setWinner", {
                        action: "success",
                        initiator: playerId,
                        data: {
                            winners: matches[roomId]["winners"]
                        }
                    });
                }
            },

            checkPodiumWinners: function(data) {
                var roomId = data.roomId;
                var winners = matches[roomId].winners;
                socketIO.to(roomId).emit("checkPodiumWinners", {
                    action: "success",
                    data: {
                        winners: winners
                    }
                });
            },

            playerDisconnect: function(socket) {
                var playerId = socket.id;

                if (playerId in players) {
                    var roomId = players[playerId].room_id;
                    if (!roomId) {
                        delete players[playerId];
                        return;
                    }

                    if (typeof matches[roomId] == "undefined") {
                        delete players[playerId];
                        return;
                    }

                    var curPlayers = matches[roomId].players;
                    var index = 0;
                    curPlayers.forEach(element => {
                        if (element.id == playerId) {
                            matches[roomId].players.splice(index, 1);
                            socketIO.to(roomId).emit("joinRoom", {
                                action: "update_players",
                                message: "Player Update!",
                                data: {
                                    room: matches[roomId]
                                }
                            });
                        }
                        index++;
                    });

                    if (typeof matches[roomId] == "undefined") {
                        delete players[playerId];
                        return;
                    }

                    if (matches[roomId].players.length < 1) {
                        delete matches[roomId];
                    }

                    delete players[playerId];

                    for (let index = 1; index < 4; index++) {
                        // console.log("IS PLAYERS HAS @BOT_" + index + " ? " + players.hasOwnProperty(playerId + "@BOT_" + index))
                        delete players[playerId + "@BOT_" + index];
                    }
                }
            }
        }
    }
}